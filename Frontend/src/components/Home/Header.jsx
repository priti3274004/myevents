import React from 'react';

export const Header = () => {
  return (
    <div className="banner"> {/* Use className instead of class for JSX */}
      <h1>Enjoy Holi Parties</h1>
    </div>
  );
};

export default Header;
