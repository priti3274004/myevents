import React from 'react';
import Header from './Home/Header';
import { Categories } from './Home/Categories';

export const Main = () => {
  return (
    <div>
      <Header />
      <Categories />
    </div>
  );
}

export default Main;
